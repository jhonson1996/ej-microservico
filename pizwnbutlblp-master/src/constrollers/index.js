import { Model } from '../models';
import { Op } from "sequelize";

import { queueTest } from '../adapter';

export const Test = async () => {
    try {

        const job = await queueTest.add();

        let { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (error) {
        console.log({ step: "controller callCreateUser", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
}

export const callNotificationsOverPhone = async ({ phone }) => {
    try {

    } catch (error) {
        console.log({ step: "controller callCreateUser", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
}

export const callCreateUser = async ({ fullName }) => {
    try {


    } catch (error) {
        console.log({ step: "controller callCreateUser", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
}

export const IfExistUsername = async ({ username }) => {
    try {

        let { statusCode } = await FindOne({ username });

        if (statusCode === 200) return { statusCode: 200, data: true };
        else return { statusCode: 200, data: false };


    } catch (error) {
        console.log({ step: "controller IfExist", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const IfExist = async ({ pk }) => {
    try {

        let { statusCode } = await FindOne({ pk });

        if (statusCode === 200) return { statusCode: 200, data: true };
        else return { statusCode: 200, data: false };


    } catch (error) {
        console.log({ step: "controller IfExist", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const FindOne = async ({ pk }) => {
    try {
        let instance = await Model.findOne({ where: { pk }, logging: false });

        if (instance) return { statusCode: 200, data: instance }
        else return { statusCode: 400, message: "Ya no existe" }

    } catch (error) {
        console.log({ step: "controller FindOne", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const View = async ({ limit, offset, where = {} }) => {

    try {

        let { rows, count } = await Model.findAndCountAll({
            where,
            logging: false,
            limit,
            offset
        });

        return {
            statusCode: 200,
            data: {
                data: rows,
                pageCount: Math.ceil(count / limit),
                itemCount: count
            }
        }

    } catch (error) {
        console.log({ step: "controller View", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tardeeee" }
    }
};

export const Update = async ({ fields, pk }) => {
    try {

        await Model.update(fields, { where: { pk }, logging: false })

        return { statusCode: 200, data: "Datos actualizados" }

    } catch (error) {
        console.log({ step: "controller Update", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const Delete = async ({ pk }) => {
    try {

        await Model.destroy({ where: { pk }, logging: false })
        return { statusCode: 200, data: "Eliminado" }

    } catch (error) {
        console.log({ step: "controller Delete", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const Create = async ({ fields }) => {
    try {

        const opts = { fields: Object.keys(fields), logging: false }

        let instance = await Model.create(fields, opts)

        if (instance) return { statusCode: 200, data: instance }

        else return { statusCode: 400, message: "Error al crear" }

    } catch (error) {
        console.log({ step: "controller Create", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
};

export const Search = async ({ search, limit, offset, exclude = [] }) => {

    try {

        var query = {
            where: {
                [Op.or]: [
                    { phone: { [Op.iLike]: `%${search}%` } },
                ]
            },
            logging: false,
            limit, offset
        }

        if (exclude.length > 0) {
            query.where.uuid = { [Op.notIn]: exclude }
        }

        let { statusCode, data, message } = await View({
            limit, offset, where: query.where
        })

        return { statusCode, data, message }

    } catch (error) {
        console.log({ step: "controller Search", error: error.toString() })
        return { statusCode: 500, message: "No podemos procesar tu solicitud intenta más tarde" }
    }
}