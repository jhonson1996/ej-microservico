import {
    queueCreate, queueDelete,
    queueFindOne, queueIfExist,
    queueUpdate, queueView,
    queueViewRegisterUser, queueSearchByPhone
} from './index';

import {
    handleCreate, handleDelete, handleFindOne,
    handleIfExist, handleUpdate, handleView,
    handleViewRegisterUser, handleSearchByPhone
} from '../services';

queueSearchByPhone.process(async (job, done) => {
    try {

        let { limit, offset, phone, exclude } = job.data;

        return done(null, await handleSearchByPhone({ limit, offset, phone, exclude }));

    } catch (error) {
        var step = "adapters queueSearchByPhone"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});

queueViewRegisterUser.process(async (job, done) => {
    try {

        let { limit, offset, registerUser } = job.data;

        return done(null, await handleViewRegisterUser({ limit, offset, registerUser }));

    } catch (error) {
        var step = "adapters queueViewRegisterUser"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});

queueDelete.process(async (job, done) => {
    try {
        let { pk } = job.data

        return done(null, await handleDelete({ pk }));

    } catch (error) {
        var step = "adapters queueDelete"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});

queueView.process(async (job, done) => {
    try {

        let { limit, offset } = job.data;

        console.log(limit, offset)

        return done(null, await handleView({ limit, offset }))

    } catch (error) {
        console.log({ step: "adapters queueView", error: error.toString() })

        return done(new Error({
            statusCode: 500,
            message: "No podemos procesar tu solicitud intenta más tarde"
        }))
    }
});

queueUpdate.process(async (job, done) => {
    try {

        let { pk, fields } = job.data

        return done(null, await handleUpdate({ pk, fields }))

    } catch (error) {
        var step = "adapters queueUpdate"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});

queueCreate.process(async (job, done) => {
    try {

        let { fields } = job.data

        return done(null, await handleCreate({ fields }))

    } catch (error) {
        var step = "adapters queueCreate"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});

queueIfExist.process(async (job, done) => {
    try {

        let { pk } = job.data

        return done(null, await handleIfExist({ pk }))

    } catch (error) {
        var step = "adapters queueIfExist"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});

queueFindOne.process(async (job, done) => {
    try {

        let { pk } = job.data

        return done(null, await handleFindOne({ pk }))

    } catch (error) {
        var step = "adapters queueFindOne"
        console.log(step, error.toString())
        return done(new Error(step))
    }
});