import Bull from "bull";
import { REDIS } from '../settings';

const opts = {
    redis: {
        host: REDIS.host,
        port: REDIS.port
    }
};

//Externals
export const queueTest = new Bull("cbbfqbvohecn:test", opts);

//Internals
export const queueFindOne = new Bull("pizwnbutlblp:findOne", opts);
export const queueIfExist = new Bull("pizwnbutlblp:ifExist", opts);
export const queueView = new Bull("pizwnbutlblp:view", opts);
export const queueUpdate = new Bull("pizwnbutlblp:update", opts);
export const queueDelete = new Bull("pizwnbutlblp:delete", opts);
export const queueCreate = new Bull("pizwnbutlblp:create", opts);
export const queueViewRegisterUser = new Bull("pizwnbutlblp:registerUser", opts);
export const queueSearchByPhone = new Bull("pizwnbutlblp:searchByPhone", opts);
