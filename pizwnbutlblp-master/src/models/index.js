import { DataTypes } from 'sequelize';
import { Sequelize } from 'sequelize';

import { DATABASES } from '../settings';

const sequelize = new Sequelize(
    DATABASES.NAME, DATABASES.USER, DATABASES.PASSWORD, {
    host: DATABASES.HOST,
    port: DATABASES.PORT,
    dialect: DATABASES.ENGINE
})

export const Model = sequelize.define('model', {
    pk: {
        type: DataTypes.UUID, primaryKey: true, unique: true,
        defaultValue: DataTypes.UUIDV4
    },
    phone: { type: DataTypes.STRING },

    fullName: { type: DataTypes.STRING },

    username: { type: DataTypes.STRING },

    image: { type: DataTypes.STRING },

    domicile: { type: DataTypes.STRING },
    zipCode: { type: DataTypes.STRING },
    email: { type: DataTypes.STRING },
    typeId: { type: DataTypes.STRING },
    idNumber: { type: DataTypes.BIGINT },
    dateOfBirth: { type: DataTypes.TIME },

    registerUser: { type: DataTypes.UUID },
    updateUser: { type: DataTypes.UUID },
}, {
    timestamps: true,
    freezeTableName: true,
});


export const syncDB = async () => {
    try {

        await Model.sync({ logging: false, force: true })

        console.log('Model up')

    } catch (error) {
        console.log('Model down')
        console.error(error)
    }
}