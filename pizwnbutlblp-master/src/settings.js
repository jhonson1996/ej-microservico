import dotenv from 'dotenv';


dotenv.config();

export const REDIS = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
};

export const DATABASES = {
    ENGINE: process.env.ENGINE,
    NAME: process.env.DB_NAME,
    USER: process.env.DB_USERNAME,
    PASSWORD: process.env.DB_PASSWORD,
    HOST: process.env.DB_HOST,
    PORT: process.env.DB_PORT,
};
