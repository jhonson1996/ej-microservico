const { createClient } = require("redis");
const dotenv = require('dotenv');

dotenv.config();

const REDIS = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
};

const subscriber = createClient({
    host: REDIS.host,
    port: REDIS.port,
})

subscriber.on("message", (channel, message) => {

    console.log(JSON.parse(message))

})

subscriber.subscribe("user-notify")