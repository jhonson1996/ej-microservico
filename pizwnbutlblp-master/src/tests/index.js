const Bull = require('bull');
const dotenv = require('dotenv');

const { createClient } = require('redis');

dotenv.config();

const REDIS = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
};

const opts = {
    redis: {
        host: REDIS.host,
        port: REDIS.port
    }
};

const redisClient = createClient({
    host: REDIS.host,
    port: REDIS.port,
})

const queueFindOne = new Bull("pizwnbutlblp:findOne", opts);
const queueIfExist = new Bull("pizwnbutlblp:ifExist", opts);
const queueView = new Bull("pizwnbutlblp:view", opts);
const queueUpdate = new Bull("pizwnbutlblp:update", opts);
const queueDelete = new Bull("pizwnbutlblp:delete", opts);
const queueCreate = new Bull("pizwnbutlblp:create", opts);
const queueViewRegisterUser = new Bull("pizwnbutlblp:registerUser", opts);
const queueSearchByPhone = new Bull("pizwnbutlblp:searchByPhone", opts);

async function SearchByPhone({ phone, limit = 12, offset = 0 }) {
    try {

        console.log("Bienvenido a SearchByPhone", opts);

        const job = await queueSearchByPhone.add({ limit, offset, phone });

        const result = await job.finished();

        console.log("result", result)

    } catch (error) {
        console.error("error", error)
    }
};

async function ViewRegisterUser({ registerUser, limit = 12, offset = 0 }) {
    try {

        console.log("Bienvenido a ViewRegisterUser", opts);
        
        const job = await queueViewRegisterUser.add({ limit, offset, registerUser });
        
        
        const result = await job.finished();

        console.log("result_R", result)

    } catch (error) {
        console.error("error", error)
    }
};

async function Publish({ pk, date }) {
    try {

        redisClient.publish("deleteUser", JSON.stringify({ pk, date }))
        console.log("deleteUser", JSON.stringify({ pk, date }))

    } catch (error) {
        console.log(error)
    }
}

async function main() {
    try {

        console.log("Bienvenido a testing--", opts);

       await ViewRegisterUser({ registerUser: '8530543b-9c7b-4c8f-a062-ce72d2a558c7' });

       //await SearchByPhone({ phone: '549261545450939' })

        /*setInterval(async () => await Publish({
            pk: '8530543b-9c7b-4c8f-a062-ce72d2a558c7',
            date: new Date()
        }), 2000);*/


    } catch (error) {
        console.error("error--", error)
    }
};

main()