const { createClient } = require("redis");
const dotenv = require('dotenv');

dotenv.config();

const REDIS = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
};


const publisher = createClient({
    host: REDIS.host,
    port: REDIS.port,
})

setInterval(() => {

    const user = {
        id: "123456",
        date: new Date(),
    }

    publisher.publish("user-notify", JSON.stringify(user));
}, 2000);